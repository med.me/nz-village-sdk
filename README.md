# Nazareth Village SDK

<p>Typescript Nazareth Village API SDK</p>

## Installation

```
npm install @nz-village/nz-village-sdk --save-dev
```
or
```
yarn add -D @nz-village/nz-village-sdk
```
## Publishing

To update publish into `npm` registry, commit and tag it with SemVer tag (e.g. `0.0.1` or `0.0.1-beta`)
