/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { AppointmentDto } from '../models/AppointmentDto';
import type { PageResponse } from '../models/PageResponse';
import type { UpdateAdminAppointmentRequest } from '../models/UpdateAdminAppointmentRequest';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class AdminService {

  /**
   * Get appointment page.
   * @param pageNumber Page number to fetch. Default is 0.
   * @param pageSize Page size. Default is 20
   * @param sort Sorting
   * @param search Search tokens
   * @param searchIn Search fields
   * @param filter Filter settings
   * @returns any
   * @throws ApiError
   */
  public static adminAppointmentControllerFindPage(
    pageNumber?: number,
    pageSize?: number,
    sort?: any,
    search?: string,
    searchIn?: any[],
    filter?: any,
  ): CancelablePromise<(PageResponse & {
    content: Array<AppointmentDto>;
  })> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/admin/appointment',
      query: {
        'pageNumber': pageNumber,
        'pageSize': pageSize,
        'sort': sort,
        'search': search,
        'searchIn': searchIn,
        'filter': filter,
      },
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
      },
    });
  }

  /**
   * Get appointment by id.
   * @param id appointment id
   * @returns AppointmentDto Appointment has been successfully fetched
   * @throws ApiError
   */
  public static adminAppointmentControllerFindOne(
    id: string,
  ): CancelablePromise<AppointmentDto> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/admin/appointment/{id}',
      path: {
        'id': id,
      },
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
        404: `Appointment with given id does not exist.`,
      },
    });
  }

  /**
   * Update appointment.
   * @param id Appointment id
   * @param requestBody
   * @returns AppointmentDto An appointment has been successfully fetched
   * @throws ApiError
   */
  public static adminAppointmentControllerUpdate(
    id: string,
    requestBody: UpdateAdminAppointmentRequest,
  ): CancelablePromise<AppointmentDto> {
    return __request(OpenAPI, {
      method: 'PUT',
      url: '/v1/admin/appointment/{id}',
      path: {
        'id': id,
      },
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
        404: `An appointment with given id does not exist.`,
      },
    });
  }

}
