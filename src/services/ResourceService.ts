/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CreateResourceRequest } from '../models/CreateResourceRequest';
import type { PageResponse } from '../models/PageResponse';
import type { ResourceDto } from '../models/ResourceDto';
import type { ResourcesScheduleDto } from '../models/ResourcesScheduleDto';
import type { UpdateResourceRequest } from '../models/UpdateResourceRequest';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class ResourceService {

  /**
   * Get resources schedule.
   * @param start Range start date in ISO-6801 format YYYY-MM-DD. Example: 2022-08-31
   * @param end Range end date in ISO-6801 format YYYY-MM-DD. Example: 2022-08-31
   * @param skip Skip for appointment Ids
   * @param resource Resource ID list
   * @returns ResourcesScheduleDto The resources schedule has been successfully fetched
   * @throws ApiError
   */
  public static resourceControllerGetSchedule(
    start: string,
    end: string,
    skip?: Array<string>,
    resource?: Array<string>,
  ): CancelablePromise<ResourcesScheduleDto> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/resource/schedule',
      query: {
        'start': start,
        'end': end,
        'skip': skip,
        'resource': resource,
      },
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
      },
    });
  }

  /**
   * Get resources by paging.
   * @param pageNumber Page number to fetch. Default is 0.
   * @param pageSize Page size. Default is 20
   * @param sort Sorting
   * @param search Search tokens
   * @param searchIn Search fields
   * @param filter Filter settings
   * @returns any
   * @throws ApiError
   */
  public static resourceControllerFindPage(
    pageNumber?: number,
    pageSize?: number,
    sort?: any,
    search?: string,
    searchIn?: any[],
    filter?: any,
  ): CancelablePromise<(PageResponse & {
    content: Array<ResourceDto>;
  })> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/resource',
      query: {
        'pageNumber': pageNumber,
        'pageSize': pageSize,
        'sort': sort,
        'search': search,
        'searchIn': searchIn,
        'filter': filter,
      },
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
      },
    });
  }

  /**
   * Create resource.
   * @param requestBody
   * @returns ResourceDto The resource has been successfully created.
   * @throws ApiError
   */
  public static resourceControllerCreate(
    requestBody: CreateResourceRequest,
  ): CancelablePromise<ResourceDto> {
    return __request(OpenAPI, {
      method: 'POST',
      url: '/v1/resource',
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
      },
    });
  }

  /**
   * Get resource by id.
   * @param id Resource id
   * @returns ResourceDto A resource has been successfully fetched
   * @throws ApiError
   */
  public static resourceControllerFindOne(
    id: string,
  ): CancelablePromise<ResourceDto> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/resource/{id}',
      path: {
        'id': id,
      },
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
        404: `A resource with given id does not exist.`,
      },
    });
  }

  /**
   * Update resource.
   * @param id resource id
   * @param requestBody
   * @returns ResourceDto The resource successfully updated.
   * @throws ApiError
   */
  public static resourceControllerUpdate(
    id: string,
    requestBody: UpdateResourceRequest,
  ): CancelablePromise<ResourceDto> {
    return __request(OpenAPI, {
      method: 'PUT',
      url: '/v1/resource/{id}',
      path: {
        'id': id,
      },
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
        404: `A resource with given id does not exist.`,
      },
    });
  }

  /**
   * Checks if the resource can have a day off on that date.
   * @param id Resource id
   * @param date Date in ISO8601 format. Example 2022-08-31
   * @returns any
   * @throws ApiError
   */
  public static resourceControllerCheckDayOffForDate(
    id: string,
    date: string,
  ): CancelablePromise<any> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/resource/{id}/day-off-check',
      path: {
        'id': id,
      },
      query: {
        'date': date,
      },
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
      },
    });
  }

}
