/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { AppointmentDto } from '../models/AppointmentDto';
import type { CreateAgentGroupTourAppointmentRequest } from '../models/CreateAgentGroupTourAppointmentRequest';
import type { CreateAgentJoinTourAppointmentRequest } from '../models/CreateAgentJoinTourAppointmentRequest';
import type { CreateAgentWaitingListGroupTourAppointmentRequest } from '../models/CreateAgentWaitingListGroupTourAppointmentRequest';
import type { CreateAgentWaitingListJoinTourAppointmentRequest } from '../models/CreateAgentWaitingListJoinTourAppointmentRequest';
import type { PageResponse } from '../models/PageResponse';
import type { UpdateAgentAppointmentRequest } from '../models/UpdateAgentAppointmentRequest';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class AgentService {

  /**
   * Get appointment by paging.
   * @param pageNumber Page number to fetch. Default is 0.
   * @param pageSize Page size. Default is 20
   * @param sort Sorting
   * @param search Search tokens
   * @param searchIn Search fields
   * @param filter Filter settings
   * @returns any
   * @throws ApiError
   */
  public static agentAppointmentControllerFindPage(
    pageNumber?: number,
    pageSize?: number,
    sort?: any,
    search?: string,
    searchIn?: any[],
    filter?: any,
  ): CancelablePromise<(PageResponse & {
    content: Array<AppointmentDto>;
  })> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/agent/appointment',
      query: {
        'pageNumber': pageNumber,
        'pageSize': pageSize,
        'sort': sort,
        'search': search,
        'searchIn': searchIn,
        'filter': filter,
      },
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
      },
    });
  }

  /**
   * Get appointment by id.
   * @param id Appointment id
   * @returns AppointmentDto An appointment has been successfully fetched
   * @throws ApiError
   */
  public static agentAppointmentControllerFindOne(
    id: string,
  ): CancelablePromise<AppointmentDto> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/agent/appointment/{id}',
      path: {
        'id': id,
      },
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
        404: `An appointment with given id does not exist.`,
      },
    });
  }

  /**
   * Update appointment.
   * @param id Appointment id
   * @param requestBody
   * @returns AppointmentDto An appointment has been successfully fetched
   * @throws ApiError
   */
  public static agentAppointmentControllerUpdate(
    id: string,
    requestBody: UpdateAgentAppointmentRequest,
  ): CancelablePromise<AppointmentDto> {
    return __request(OpenAPI, {
      method: 'PUT',
      url: '/v1/agent/appointment/{id}',
      path: {
        'id': id,
      },
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
        404: `An appointment with given id does not exist.`,
      },
    });
  }

  /**
   * Create agent's JOIN_A_TOUR appointment.
   * @param requestBody
   * @returns AppointmentDto The appointment has been successfully created.
   * @throws ApiError
   */
  public static agentAppointmentControllerCreateJoinTourAppointment(
    requestBody: CreateAgentJoinTourAppointmentRequest,
  ): CancelablePromise<AppointmentDto> {
    return __request(OpenAPI, {
      method: 'POST',
      url: '/v1/agent/appointment/join_a_tour',
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
      },
    });
  }

  /**
   * Add agent's JOIN_A_TOUR appointment to the waiting list.
   * @param requestBody
   * @returns AppointmentDto The appointment has been successfully added to the waiting list.
   * @throws ApiError
   */
  public static agentAppointmentControllerCreateWaitingListJoinTourAppointment(
    requestBody: CreateAgentWaitingListJoinTourAppointmentRequest,
  ): CancelablePromise<AppointmentDto> {
    return __request(OpenAPI, {
      method: 'POST',
      url: '/v1/agent/appointment/join_a_tour/waiting',
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
      },
    });
  }

  /**
   * Create agent's GROUP_TOUR appointments.
   * @param requestBody
   * @returns AppointmentDto Appointments have been successfully created.
   * @throws ApiError
   */
  public static agentAppointmentControllerCreateGroupTourAppointments(
    requestBody: CreateAgentGroupTourAppointmentRequest,
  ): CancelablePromise<Array<AppointmentDto>> {
    return __request(OpenAPI, {
      method: 'POST',
      url: '/v1/agent/appointment/group_tour',
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
      },
    });
  }

  /**
   * Add agent's GROUP_TOUR appointments to the waiting list.
   * @param requestBody
   * @returns AppointmentDto Appointments have been successfully added to the waiting list.
   * @throws ApiError
   */
  public static agentAppointmentControllerCreateWaitingListGroupTourAppointments(
    requestBody: CreateAgentWaitingListGroupTourAppointmentRequest,
  ): CancelablePromise<Array<AppointmentDto>> {
    return __request(OpenAPI, {
      method: 'POST',
      url: '/v1/agent/appointment/group_tour/waiting',
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
      },
    });
  }

}
