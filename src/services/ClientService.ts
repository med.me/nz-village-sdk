/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ClientDto } from '../models/ClientDto';
import type { CreateClientRequest } from '../models/CreateClientRequest';
import type { PageResponse } from '../models/PageResponse';
import type { UpdateClientRequest } from '../models/UpdateClientRequest';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class ClientService {

  /**
   * Get clients by paging.
   * @param pageNumber Page number to fetch. Default is 0.
   * @param pageSize Page size. Default is 20
   * @param sort Sorting
   * @param search Search tokens
   * @param searchIn Search fields
   * @param filter Filter settings
   * @returns any
   * @throws ApiError
   */
  public static clientControllerFindPage(
    pageNumber?: number,
    pageSize?: number,
    sort?: any,
    search?: string,
    searchIn?: any[],
    filter?: any,
  ): CancelablePromise<(PageResponse & {
    content: Array<ClientDto>;
  })> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/client',
      query: {
        'pageNumber': pageNumber,
        'pageSize': pageSize,
        'sort': sort,
        'search': search,
        'searchIn': searchIn,
        'filter': filter,
      },
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
      },
    });
  }

  /**
   * Create client.
   * @param requestBody
   * @returns ClientDto The client has been successfully created.
   * @throws ApiError
   */
  public static clientControllerCreate(
    requestBody: CreateClientRequest,
  ): CancelablePromise<ClientDto> {
    return __request(OpenAPI, {
      method: 'POST',
      url: '/v1/client',
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
      },
    });
  }

  /**
   * Get client by id.
   * @param id Client id
   * @returns ClientDto A client has been successfully fetched
   * @throws ApiError
   */
  public static clientControllerFindOne(
    id: string,
  ): CancelablePromise<ClientDto> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/client/{id}',
      path: {
        'id': id,
      },
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
        404: `A client with given id does not exist.`,
      },
    });
  }

  /**
   * Update client.
   * @param id Client id
   * @param requestBody
   * @returns any The client successfully updated.
   * @throws ApiError
   */
  public static clientControllerUpdate(
    id: string,
    requestBody: UpdateClientRequest,
  ): CancelablePromise<any> {
    return __request(OpenAPI, {
      method: 'PUT',
      url: '/v1/client/{id}',
      path: {
        'id': id,
      },
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
        404: `A client with given id does not exist.`,
      },
    });
  }

}
