/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { GroupTourDateSlots } from '../models/GroupTourDateSlots';
import type { ResourceDateSlots } from '../models/ResourceDateSlots';
import type { ScheduleDateSlots } from '../models/ScheduleDateSlots';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class TimeslotService {

  /**
   * Get JOIN_A_TOUR time slots.
   * @param start Range start date in ISO-6801 format YYYY-MM-DD. Example: 2022-08-31
   * @param end Range end date in ISO-6801 format YYYY-MM-DD. Example: 2022-08-31
   * @param requiredCapacity Tour required capacity
   * @param skip Skip for appointment Ids
   * @param resource Resource ID list
   * @param withLunch Receive slots with or without lunch
   * @returns ScheduleDateSlots Join a tour timeslots have been successfully fetched
   * @throws ApiError
   */
  public static timeslotControllerGetJoinTourSlots(
    start: string,
    end: string,
    requiredCapacity: number,
    skip?: Array<string>,
    resource?: Array<string>,
    withLunch: boolean = false,
  ): CancelablePromise<Array<ScheduleDateSlots>> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/timeslot/join_a_tour',
      query: {
        'start': start,
        'end': end,
        'skip': skip,
        'resource': resource,
        'withLunch': withLunch,
        'requiredCapacity': requiredCapacity,
      },
      errors: {
        400: `Request fields validation failed.`,
      },
    });
  }

  /**
   * Get GROUP_TOUR time slots.
   * @param start Range start date in ISO-6801 format YYYY-MM-DD. Example: 2022-08-31
   * @param end Range end date in ISO-6801 format YYYY-MM-DD. Example: 2022-08-31
   * @param requiredBusCount Number of busses
   * @param requiredCapacity Tour required capacity, pax
   * @param skip Skip for appointment Ids
   * @param resource Resource ID list
   * @param withLunch Receive slots with or without lunch
   * @returns GroupTourDateSlots Group tour timeslots have been successfully fetched
   * @throws ApiError
   */
  public static timeslotControllerGetGroupTourSlots(
    start: string,
    end: string,
    requiredBusCount: number,
    requiredCapacity: number,
    skip?: Array<string>,
    resource?: Array<string>,
    withLunch: boolean = false,
  ): CancelablePromise<Array<GroupTourDateSlots>> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/timeslot/group_tour',
      query: {
        'start': start,
        'end': end,
        'skip': skip,
        'resource': resource,
        'withLunch': withLunch,
        'requiredBusCount': requiredBusCount,
        'requiredCapacity': requiredCapacity,
      },
      errors: {
        400: `Request fields validation failed.`,
      },
    });
  }

  /**
   * Get time slots where we can move appointment
   * @param start Range start date in ISO-6801 format YYYY-MM-DD. Example: 2022-08-31
   * @param end Range end date in ISO-6801 format YYYY-MM-DD. Example: 2022-08-31
   * @param appointmentId Appointment ID
   * @param requiredCapacity Tour required capacity
   * @param skip Skip for appointment Ids
   * @param resource Resource ID list
   * @param withLunch Receive slots with or without lunch
   * @param strictResource Limit response with current appointment resource
   * @returns ResourceDateSlots Available timeslots have been successfully fetched
   * @throws ApiError
   */
  public static timeslotControllerGetAppointmentMovementSlots(
    start: string,
    end: string,
    appointmentId: string,
    requiredCapacity: number,
    skip?: Array<string>,
    resource?: Array<string>,
    withLunch: boolean = false,
    strictResource?: boolean,
  ): CancelablePromise<Array<ResourceDateSlots>> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/timeslot/move',
      query: {
        'start': start,
        'end': end,
        'skip': skip,
        'resource': resource,
        'appointmentId': appointmentId,
        'withLunch': withLunch,
        'requiredCapacity': requiredCapacity,
        'strictResource': strictResource,
      },
      errors: {
        400: `Request fields validation failed.`,
      },
    });
  }

}
