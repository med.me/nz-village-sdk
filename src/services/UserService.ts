/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CreateUserRequest } from '../models/CreateUserRequest';
import type { PageResponse } from '../models/PageResponse';
import type { UpdateUserRequest } from '../models/UpdateUserRequest';
import type { UserDto } from '../models/UserDto';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class UserService {

  /**
   * Get users by paging.
   * @param pageNumber Page number to fetch. Default is 0.
   * @param pageSize Page size. Default is 20
   * @param sort Sorting
   * @param search Search tokens
   * @param searchIn Search fields
   * @param filter Filter settings
   * @returns any
   * @throws ApiError
   */
  public static userControllerFindPage(
    pageNumber?: number,
    pageSize?: number,
    sort?: any,
    search?: string,
    searchIn?: any[],
    filter?: any,
  ): CancelablePromise<(PageResponse & {
    content: Array<UserDto>;
  })> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/user',
      query: {
        'pageNumber': pageNumber,
        'pageSize': pageSize,
        'sort': sort,
        'search': search,
        'searchIn': searchIn,
        'filter': filter,
      },
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
      },
    });
  }

  /**
   * Create user.
   * @param requestBody
   * @returns UserDto The user has been successfully created.
   * @throws ApiError
   */
  public static userControllerCreate(
    requestBody: CreateUserRequest,
  ): CancelablePromise<UserDto> {
    return __request(OpenAPI, {
      method: 'POST',
      url: '/v1/user',
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
        409: `Username is taken by another user.`,
      },
    });
  }

  /**
   * Get user by id.
   * @param id User id
   * @returns UserDto The user has been successfully fetched
   * @throws ApiError
   */
  public static userControllerFindOne(
    id: string,
  ): CancelablePromise<UserDto> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/user/{id}',
      path: {
        'id': id,
      },
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
        404: `A user with given id does not exist.`,
      },
    });
  }

  /**
   * Update user.
   * @param id User id
   * @param requestBody
   * @returns UserDto The user has been successfully updated
   * @throws ApiError
   */
  public static userControllerUpdate(
    id: string,
    requestBody: UpdateUserRequest,
  ): CancelablePromise<UserDto> {
    return __request(OpenAPI, {
      method: 'PUT',
      url: '/v1/user/{id}',
      path: {
        'id': id,
      },
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
        404: `A user with given id does not exist.`,
      },
    });
  }

}
