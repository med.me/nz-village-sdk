/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { PublicJoinTourSettingsDto } from '../models/PublicJoinTourSettingsDto';
import type { UpdateVillageSettingsRequest } from '../models/UpdateVillageSettingsRequest';
import type { VillageSettingsDto } from '../models/VillageSettingsDto';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class SettingsService {

  /**
   * Get village settings.
   * @returns VillageSettingsDto Village settings has been successfully fetched.
   * @throws ApiError
   */
  public static settingsControllerGetSettings(): CancelablePromise<VillageSettingsDto> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/settings',
      errors: {
        401: `Current user has no permission for the operation.`,
      },
    });
  }

  /**
   * Update village settings.
   * @param requestBody
   * @returns VillageSettingsDto Village settings has been successfully updated
   * @throws ApiError
   */
  public static settingsControllerUpdateSettings(
    requestBody: UpdateVillageSettingsRequest,
  ): CancelablePromise<VillageSettingsDto> {
    return __request(OpenAPI, {
      method: 'PUT',
      url: '/v1/settings',
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
      },
    });
  }

  /**
   * Get village timezone.
   * @returns string Village timezone has been successfully fetched.
   * @throws ApiError
   */
  public static settingsControllerGetTimezone(): CancelablePromise<string> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/settings/timezone',
    });
  }

  /**
   * Get JOIN TOUR public settings.
   * @returns PublicJoinTourSettingsDto JOIN TOUR public settings has been successfully fetched.
   * @throws ApiError
   */
  public static settingsControllerGetJoinTourAvailability(): CancelablePromise<PublicJoinTourSettingsDto> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/settings/join-tour',
    });
  }

}
