/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { AppointmentDto } from '../models/AppointmentDto';
import type { AppointmentPriceDto } from '../models/AppointmentPriceDto';
import type { ClientAppointmentPriceCalculationRequest } from '../models/ClientAppointmentPriceCalculationRequest';
import type { CreateAgentGroupTourAppointmentRequest } from '../models/CreateAgentGroupTourAppointmentRequest';
import type { CreateAgentJoinTourAppointmentRequest } from '../models/CreateAgentJoinTourAppointmentRequest';
import type { CreateAgentWaitingListGroupTourAppointmentRequest } from '../models/CreateAgentWaitingListGroupTourAppointmentRequest';
import type { CreateAgentWaitingListJoinTourAppointmentRequest } from '../models/CreateAgentWaitingListJoinTourAppointmentRequest';
import type { CreateClientGroupTourAppointmentRequest } from '../models/CreateClientGroupTourAppointmentRequest';
import type { CreateClientJoinTourAppointmentRequest } from '../models/CreateClientJoinTourAppointmentRequest';
import type { PageResponse } from '../models/PageResponse';
import type { UpdateAdminAppointmentRequest } from '../models/UpdateAdminAppointmentRequest';
import type { UpdateAgentAppointmentRequest } from '../models/UpdateAgentAppointmentRequest';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class AppointmentService {

  /**
   * Create client JOIN_A_TOUR appointment.
   * @param requestBody
   * @returns AppointmentDto The appointment has been successfully created.
   * @throws ApiError
   */
  public static clientAppointmentControllerCreateJoinTour(
    requestBody: CreateClientJoinTourAppointmentRequest,
  ): CancelablePromise<AppointmentDto> {
    return __request(OpenAPI, {
      method: 'POST',
      url: '/v1/client/appointment/join_a_tour',
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
      },
    });
  }

  /**
   * Create client GROUP_TOUR appointment.
   * @param requestBody
   * @returns AppointmentDto The appointment has been successfully created.
   * @throws ApiError
   */
  public static clientAppointmentControllerCreateGroupTour(
    requestBody: CreateClientGroupTourAppointmentRequest,
  ): CancelablePromise<Array<AppointmentDto>> {
    return __request(OpenAPI, {
      method: 'POST',
      url: '/v1/client/appointment/group_tour',
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
      },
    });
  }

  /**
   * Calculate price for an appointment.
   * @param requestBody
   * @returns AppointmentPriceDto The appointment price has been successfully calculated.
   * @throws ApiError
   */
  public static clientAppointmentControllerCalculatePrice(
    requestBody: ClientAppointmentPriceCalculationRequest,
  ): CancelablePromise<AppointmentPriceDto> {
    return __request(OpenAPI, {
      method: 'PUT',
      url: '/v1/client/appointment/price',
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
      },
    });
  }

  /**
   * Get appointment by paging.
   * @param pageNumber Page number to fetch. Default is 0.
   * @param pageSize Page size. Default is 20
   * @param sort Sorting
   * @param search Search tokens
   * @param searchIn Search fields
   * @param filter Filter settings
   * @returns any
   * @throws ApiError
   */
  public static agentAppointmentControllerFindPage(
    pageNumber?: number,
    pageSize?: number,
    sort?: any,
    search?: string,
    searchIn?: any[],
    filter?: any,
  ): CancelablePromise<(PageResponse & {
    content: Array<AppointmentDto>;
  })> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/agent/appointment',
      query: {
        'pageNumber': pageNumber,
        'pageSize': pageSize,
        'sort': sort,
        'search': search,
        'searchIn': searchIn,
        'filter': filter,
      },
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
      },
    });
  }

  /**
   * Get appointment by id.
   * @param id Appointment id
   * @returns AppointmentDto An appointment has been successfully fetched
   * @throws ApiError
   */
  public static agentAppointmentControllerFindOne(
    id: string,
  ): CancelablePromise<AppointmentDto> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/agent/appointment/{id}',
      path: {
        'id': id,
      },
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
        404: `An appointment with given id does not exist.`,
      },
    });
  }

  /**
   * Update appointment.
   * @param id Appointment id
   * @param requestBody
   * @returns AppointmentDto An appointment has been successfully fetched
   * @throws ApiError
   */
  public static agentAppointmentControllerUpdate(
    id: string,
    requestBody: UpdateAgentAppointmentRequest,
  ): CancelablePromise<AppointmentDto> {
    return __request(OpenAPI, {
      method: 'PUT',
      url: '/v1/agent/appointment/{id}',
      path: {
        'id': id,
      },
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
        404: `An appointment with given id does not exist.`,
      },
    });
  }

  /**
   * Create agent's JOIN_A_TOUR appointment.
   * @param requestBody
   * @returns AppointmentDto The appointment has been successfully created.
   * @throws ApiError
   */
  public static agentAppointmentControllerCreateJoinTourAppointment(
    requestBody: CreateAgentJoinTourAppointmentRequest,
  ): CancelablePromise<AppointmentDto> {
    return __request(OpenAPI, {
      method: 'POST',
      url: '/v1/agent/appointment/join_a_tour',
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
      },
    });
  }

  /**
   * Add agent's JOIN_A_TOUR appointment to the waiting list.
   * @param requestBody
   * @returns AppointmentDto The appointment has been successfully added to the waiting list.
   * @throws ApiError
   */
  public static agentAppointmentControllerCreateWaitingListJoinTourAppointment(
    requestBody: CreateAgentWaitingListJoinTourAppointmentRequest,
  ): CancelablePromise<AppointmentDto> {
    return __request(OpenAPI, {
      method: 'POST',
      url: '/v1/agent/appointment/join_a_tour/waiting',
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
      },
    });
  }

  /**
   * Create agent's GROUP_TOUR appointments.
   * @param requestBody
   * @returns AppointmentDto Appointments have been successfully created.
   * @throws ApiError
   */
  public static agentAppointmentControllerCreateGroupTourAppointments(
    requestBody: CreateAgentGroupTourAppointmentRequest,
  ): CancelablePromise<Array<AppointmentDto>> {
    return __request(OpenAPI, {
      method: 'POST',
      url: '/v1/agent/appointment/group_tour',
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
      },
    });
  }

  /**
   * Add agent's GROUP_TOUR appointments to the waiting list.
   * @param requestBody
   * @returns AppointmentDto Appointments have been successfully added to the waiting list.
   * @throws ApiError
   */
  public static agentAppointmentControllerCreateWaitingListGroupTourAppointments(
    requestBody: CreateAgentWaitingListGroupTourAppointmentRequest,
  ): CancelablePromise<Array<AppointmentDto>> {
    return __request(OpenAPI, {
      method: 'POST',
      url: '/v1/agent/appointment/group_tour/waiting',
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
      },
    });
  }

  /**
   * Get appointment page.
   * @param pageNumber Page number to fetch. Default is 0.
   * @param pageSize Page size. Default is 20
   * @param sort Sorting
   * @param search Search tokens
   * @param searchIn Search fields
   * @param filter Filter settings
   * @returns any
   * @throws ApiError
   */
  public static adminAppointmentControllerFindPage(
    pageNumber?: number,
    pageSize?: number,
    sort?: any,
    search?: string,
    searchIn?: any[],
    filter?: any,
  ): CancelablePromise<(PageResponse & {
    content: Array<AppointmentDto>;
  })> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/admin/appointment',
      query: {
        'pageNumber': pageNumber,
        'pageSize': pageSize,
        'sort': sort,
        'search': search,
        'searchIn': searchIn,
        'filter': filter,
      },
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
      },
    });
  }

  /**
   * Get appointment by id.
   * @param id appointment id
   * @returns AppointmentDto Appointment has been successfully fetched
   * @throws ApiError
   */
  public static adminAppointmentControllerFindOne(
    id: string,
  ): CancelablePromise<AppointmentDto> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/admin/appointment/{id}',
      path: {
        'id': id,
      },
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
        404: `Appointment with given id does not exist.`,
      },
    });
  }

  /**
   * Update appointment.
   * @param id Appointment id
   * @param requestBody
   * @returns AppointmentDto An appointment has been successfully fetched
   * @throws ApiError
   */
  public static adminAppointmentControllerUpdate(
    id: string,
    requestBody: UpdateAdminAppointmentRequest,
  ): CancelablePromise<AppointmentDto> {
    return __request(OpenAPI, {
      method: 'PUT',
      url: '/v1/admin/appointment/{id}',
      path: {
        'id': id,
      },
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
        404: `An appointment with given id does not exist.`,
      },
    });
  }

}
