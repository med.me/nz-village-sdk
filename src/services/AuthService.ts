/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { AuthLogin } from '../models/AuthLogin';
import type { TokenResponse } from '../models/TokenResponse';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class AuthService {

  /**
   * User login.
   * @param requestBody User login information
   * @returns TokenResponse User login success.
   * @throws ApiError
   */
  public static authControllerLogin(
    requestBody: AuthLogin,
  ): CancelablePromise<TokenResponse> {
    return __request(OpenAPI, {
      method: 'POST',
      url: '/v1/auth/login',
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        401: `User login failed.`,
      },
    });
  }

  /**
   * Refresh token.
   * @returns TokenResponse Refresh token success.
   * @throws ApiError
   */
  public static authControllerRefresh(): CancelablePromise<TokenResponse> {
    return __request(OpenAPI, {
      method: 'POST',
      url: '/v1/auth/refresh',
      errors: {
        401: `Refresh token failed.`,
      },
    });
  }

}
