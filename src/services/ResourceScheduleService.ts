/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { EntityScheduleDto } from '../models/EntityScheduleDto';
import type { UpdateResourceScheduleRequest } from '../models/UpdateResourceScheduleRequest';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class ResourceScheduleService {

  /**
   * Get resource schedule.
   * @param id Resource id
   * @param start Range start date in ISO-6801 format YYYY-MM-DD. Example: 2022-08-31
   * @param end Range end date in ISO-6801 format YYYY-MM-DD. Example: 2022-08-31
   * @param skip Skip for appointment Ids
   * @param resource Resource ID list
   * @returns EntityScheduleDto The resource schedule has been successfully fetched
   * @throws ApiError
   */
  public static resourceScheduleControllerGet(
    id: string,
    start: string,
    end: string,
    skip?: Array<string>,
    resource?: Array<string>,
  ): CancelablePromise<Array<EntityScheduleDto>> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/resource/{id}/schedule',
      path: {
        'id': id,
      },
      query: {
        'start': start,
        'end': end,
        'skip': skip,
        'resource': resource,
      },
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
        403: `Only resources of type INTERNAL_GUIDE are acceptable`,
        404: `A resource with given id does not exist.`,
      },
    });
  }

  /**
   * Update resource schedule.
   * @param id Resource id
   * @param requestBody
   * @returns any The resource schedule has been successfully updated
   * @throws ApiError
   */
  public static resourceScheduleControllerSet(
    id: string,
    requestBody: UpdateResourceScheduleRequest,
  ): CancelablePromise<any> {
    return __request(OpenAPI, {
      method: 'PUT',
      url: '/v1/resource/{id}/schedule',
      path: {
        'id': id,
      },
      body: requestBody,
      mediaType: 'application/json',
      errors: {
        400: `Request data validation failed.`,
        401: `Current user has no permission for the operation.`,
        403: `Only resources of type INTERNAL_GUIDE are acceptable`,
        404: `A resource with given id does not exist.`,
      },
    });
  }

}
