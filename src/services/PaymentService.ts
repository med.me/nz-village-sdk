/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class PaymentService {

  /**
   * Provider success callback.
   * @param id Payment Id
   * @returns any The payment has been successfully made
   * @throws ApiError
   */
  public static paymentControllerCallbackSuccess(
    id: string,
  ): CancelablePromise<any> {
    return __request(OpenAPI, {
      method: 'POST',
      url: '/v1/payment/callback/{id}/success',
      path: {
        'id': id,
      },
      errors: {
        400: `Request data validation failed.`,
        404: `A payment with given id does not exist.`,
      },
    });
  }

  /**
   * Notify about tour payment error.
   * @param id Payment Id
   * @returns any The payment has been successfully paid
   * @throws ApiError
   */
  public static paymentControllerCallbackError(
    id: string,
  ): CancelablePromise<any> {
    return __request(OpenAPI, {
      method: 'POST',
      url: '/v1/payment/callback/{id}/error',
      path: {
        'id': id,
      },
      errors: {
        400: `Request data validation failed.`,
        404: `A payment with given id does not exist.`,
      },
    });
  }

  /**
   * @returns any
   * @throws ApiError
   */
  public static paymentControllerRedirectSuccess(): CancelablePromise<any> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/payment/{id}/success',
      errors: {
        400: `Request data validation failed.`,
      },
    });
  }

  /**
   * @returns any
   * @throws ApiError
   */
  public static paymentControllerRedirectCancel(): CancelablePromise<any> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/payment/{id}/cancel',
      errors: {
        400: `Request data validation failed.`,
      },
    });
  }

  /**
   * @returns any
   * @throws ApiError
   */
  public static paymentControllerRedirectError(): CancelablePromise<any> {
    return __request(OpenAPI, {
      method: 'GET',
      url: '/v1/payment/{id}/error',
      errors: {
        400: `Request data validation failed.`,
      },
    });
  }

  /**
   * Check "ready for pay" payment status.
   * @param id Payment Id
   * @returns boolean Returns "ready for pay" status. If true - it's ready for payment and false otherwise.
   * @throws ApiError
   */
  public static paymentControllerGetStatus(
    id: string,
  ): CancelablePromise<boolean> {
    return __request(OpenAPI, {
      method: 'PUT',
      url: '/v1/payment/{id}/status',
      path: {
        'id': id,
      },
      errors: {
        400: `Request data validation failed.`,
        404: `A payment with given id does not exist or expired.`,
      },
    });
  }

}
