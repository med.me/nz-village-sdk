/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EmailNotificationSettingsDto } from './EmailNotificationSettingsDto';

export type NotificationSettingsDto = {
  /**
   * Client email notification
   */
  clientEmailNotification: EmailNotificationSettingsDto;
  /**
   * Agent email notification
   */
  agentEmailNotification: EmailNotificationSettingsDto;
  /**
   * Agent waiting list email notification
   */
  agentWaitingListEmailNotification: EmailNotificationSettingsDto;
  /**
   * Agent cancel tour email notification
   */
  agentOnCancelTourEmailNotification: EmailNotificationSettingsDto;
  /**
   * Agent change tour (lunch, slot time) email notification
   */
  agentOnChangeTourEmailNotification: EmailNotificationSettingsDto;
};

