/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CreateClientDto } from './CreateClientDto';

export type CreateClientRequest = {
  /**
   * Client data
   */
  client: CreateClientDto;
};

