/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppointmentGroupTourVisitorsDto } from './AppointmentGroupTourVisitorsDto';
import type { AppointmentGuideDto } from './AppointmentGuideDto';

export type CreateAgentWaitingListGroupTourAppointmentDto = {
  /**
   * Appointment start date and time in ISO8601 UTC format
   */
  start: string;
  /**
   * Appointment end date and time in ISO8601 UTC format
   */
  end: string;
  /**
   * Group name
   */
  groupName?: string;
  /**
   * Country of origin
   */
  originCountry: Array<any[]>;
  /**
   * Include lunch or not
   */
  withLunch: boolean;
  /**
   * Include filming or not
   */
  withFilming: boolean;
  /**
   * Number of visitors.
   */
  visitors: AppointmentGroupTourVisitorsDto;
  /**
   * Appointment guide
   */
  guide?: AppointmentGuideDto;
  /**
   * Tour agent notes
   */
  agentNotes?: string;
};

