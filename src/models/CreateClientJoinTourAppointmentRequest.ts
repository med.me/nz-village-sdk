/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CreateClientJoinTourAppointmentDto } from './CreateClientJoinTourAppointmentDto';

export type CreateClientJoinTourAppointmentRequest = {
  /**
   * Create JOIN_A_TOUR appointment
   */
  appointment: CreateClientJoinTourAppointmentDto;
};

