/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CreateClientGroupTourAppointmentDto } from './CreateClientGroupTourAppointmentDto';

export type CreateClientGroupTourAppointmentRequest = {
  /**
   * Create GROUP_TOUR appointment
   */
  appointment: CreateClientGroupTourAppointmentDto;
};

