/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PersonTourPriceDto = {
  /**
   * Tour prices without lunch in cents. Zero === no price.
   */
  personType: PersonTourPriceDto.personType;
  /**
   * Tour prices without lunch in cents. Zero === no price.
   */
  price: number;
};

export namespace PersonTourPriceDto {

  /**
   * Tour prices without lunch in cents. Zero === no price.
   */
  export enum personType {
    ADULT = 'ADULT',
    SENIOR = 'SENIOR',
    CHILD_C1 = 'CHILD_C1',
    CHILD_C2 = 'CHILD_C2',
  }


}

