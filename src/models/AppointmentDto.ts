/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppointmentClientDto } from './AppointmentClientDto';
import type { AppointmentGuideDto } from './AppointmentGuideDto';
import type { AppointmentPriceDto } from './AppointmentPriceDto';
import type { AppointmentVisitorsDto } from './AppointmentVisitorsDto';
import type { ResourceDto } from './ResourceDto';
import type { UserDto } from './UserDto';

export type AppointmentDto = {
  /**
   * Creation time
   */
  readonly createdAt: string;
  /**
   * Time of last update
   */
  readonly updatedAt: string;
  /**
   * Identifier
   */
  id: string;
  /**
   * Appointment number
   */
  readonly number?: number;
  /**
   * Appointment tour group number
   */
  readonly groupNumber?: number;
  /**
   * Appointment start date and time in ISO8601 UTC format
   */
  start: string;
  /**
   * Appointment end date and time in ISO8601 UTC format
   */
  end: string;
  /**
   * Tour lunch time if tour with lunch. Time in ISO8601 UTC format
   */
  readonly lunchTime?: string;
  /**
   * Appointment resource. Guide, bus etc.
   */
  resource: ResourceDto;
  /**
   * Appointment client contact person
   */
  contactPerson?: AppointmentClientDto;
  /**
   * Appointment guide
   */
  guide?: AppointmentGuideDto;
  /**
   * Appointment status
   */
  status?: AppointmentDto.status;
  /**
   * Booking type
   */
  bookingType: AppointmentDto.bookingType;
  /**
   * Number of visitors
   */
  visitors: AppointmentVisitorsDto;
  /**
   * Group name
   */
  groupName?: string;
  /**
   * Country of origin
   */
  originCountry: Array<any[]>;
  /**
   * Include lunch or not
   */
  withLunch: boolean;
  /**
   * Include filming or not
   */
  withFilming: boolean;
  /**
   * Price
   */
  price?: AppointmentPriceDto;
  /**
   * Created user
   */
  readonly createdBy?: UserDto;
  /**
   * Online payment provider URL
   */
  paymentUrl?: string;
  /**
   * Online payment expiration timeout, in minutes
   */
  expiredIn?: number;
  /**
   * Payment
   */
  payment?: string;
  /**
   * Tour notes
   */
  notes?: string;
  /**
   * Tour agent notes
   */
  agentNotes?: string;
};

export namespace AppointmentDto {

  /**
   * Appointment status
   */
  export enum status {
    CONFIRMED = 'CONFIRMED',
    WAITING_LIST = 'WAITING_LIST',
    WAITING_FOR_PAYMENT = 'WAITING_FOR_PAYMENT',
    PAYMENT_FAILED = 'PAYMENT_FAILED',
    CANCELED = 'CANCELED',
    DELETED = 'DELETED',
  }

  /**
   * Booking type
   */
  export enum bookingType {
    JOIN_A_TOUR = 'JOIN_A_TOUR',
    GROUP_TOUR = 'GROUP_TOUR',
  }


}

