/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppointmentTouristVisitorsDto = {
  /**
   * Number of adult visitors
   */
  adultsNumber: number;
  /**
   * Number of seniors or students
   */
  seniorsNumber: number;
  /**
   * Number of children under 5 y.o.
   */
  childrenCat1Number: number;
  /**
   * Number of children 5-12 y.o.
   */
  childrenCat2Number: number;
};

