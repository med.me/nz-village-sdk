/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TimeRangeDto = {
  /**
   * Range start in minutes
   */
  start: number;
  /**
   * Range end in minutes
   */
  end: number;
};

