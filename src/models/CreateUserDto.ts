/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UserRole } from './UserRole';

export type CreateUserDto = {
  /**
   * User name, minimum 3 characters long, only letters, digits, "@" and "-" allowed
   */
  username: string;
  /**
   * User email
   */
  email: string;
  /**
   * User agency
   */
  agency?: string;
  /**
   * User full name
   */
  fullName?: string;
  /**
   * User permission roles
   */
  roles: Array<UserRole>;
  /**
   * User password
   */
  password: string;
};

