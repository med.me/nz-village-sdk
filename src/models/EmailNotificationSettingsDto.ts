/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type EmailNotificationSettingsDto = {
  /**
   * Status of notification
   */
  active: boolean;
  /**
   * Email notification subject
   */
  subject?: any;
  /**
   * Email notification message
   */
  message?: any;
};

