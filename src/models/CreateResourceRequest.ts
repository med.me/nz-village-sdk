/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CreateResourceDto } from './CreateResourceDto';

export type CreateResourceRequest = {
  /**
   * Resource data
   */
  resource: CreateResourceDto;
};

