/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PublicJoinTourSettingsDto = {
  /**
   * Hide booking form
   */
  hideBookingForm: boolean;
};

