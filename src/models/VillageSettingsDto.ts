/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { GroupTourSettingsDto } from './GroupTourSettingsDto';
import type { JoinTourSettingsDto } from './JoinTourSettingsDto';
import type { NotificationSettingsDto } from './NotificationSettingsDto';
import type { PaymentProviderSettingsDto } from './PaymentProviderSettingsDto';
import type { ScheduleSettingsDto } from './ScheduleSettingsDto';

export type VillageSettingsDto = {
  /**
   * Slot size in minutes
   */
  slotSize: number;
  /**
   * Village work schedule
   */
  schedule: ScheduleSettingsDto;
  /**
   * Join tour settings
   */
  joinTour: JoinTourSettingsDto;
  /**
   * Group tour settings
   */
  groupTour: GroupTourSettingsDto;
  /**
   * Payment provider settings
   */
  payment: PaymentProviderSettingsDto;
  /**
   * Email notification settings
   */
  notification: NotificationSettingsDto;
};

