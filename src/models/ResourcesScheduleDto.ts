/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ResourceScheduleDto } from './ResourceScheduleDto';
import type { WeekScheduleDto } from './WeekScheduleDto';

export type ResourcesScheduleDto = {
  /**
   * Slot size in minutes
   */
  slotSize: number;
  /**
   * Village schedule
   */
  villageSchedule: WeekScheduleDto;
  /**
   * Resources schedule
   */
  resourceSchedule: Array<ResourceScheduleDto>;
};

