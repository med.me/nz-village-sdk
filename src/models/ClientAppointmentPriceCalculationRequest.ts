/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppointmentVisitorsDto } from './AppointmentVisitorsDto';

export type ClientAppointmentPriceCalculationRequest = {
  /**
   * Number of visitors
   */
  visitors: AppointmentVisitorsDto;
  /**
   * Include filming or not
   */
  withFilming?: boolean;
};

