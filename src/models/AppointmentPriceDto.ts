/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppointmentTouristPriceDto } from './AppointmentTouristPriceDto';

export type AppointmentPriceDto = {
  /**
   * Prices for local tourists
   */
  localPrices: AppointmentTouristPriceDto;
  /**
   * Prices for foreign tourists
   */
  foreignPrices: AppointmentTouristPriceDto;
  /**
   * Price for filming
   */
  filmingPrice: number;
  /**
   * Paid amount
   */
  readonly paidTotal?: number;
};

