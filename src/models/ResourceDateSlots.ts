/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ResourceSlots } from './ResourceSlots';

export type ResourceDateSlots = {
  /**
   * Time slots date in ISO8601 format. Example 2022-08-31
   */
  date: string;
  /**
   * Resources with slots specific date
   */
  resources: Array<ResourceSlots>;
};

