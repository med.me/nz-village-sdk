/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type JoinTourTimeRangeDto = {
  /**
   * Range start in minutes
   */
  start: number;
  /**
   * Range end in minutes
   */
  end: number;
  /**
   * Max person slot capacity
   */
  capacity: number;
  /**
   * Include lunch
   */
  withLunch: boolean;
  /**
   * Join a tour period start date in ISO8601 UTC format
   */
  startDate?: string;
  /**
   * Join a tour period end date in ISO8601 UTC format
   */
  endDate?: string;
};

