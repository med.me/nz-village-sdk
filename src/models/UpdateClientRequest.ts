/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UpdateClientDto } from './UpdateClientDto';

export type UpdateClientRequest = {
  /**
   * Client data
   */
  client: UpdateClientDto;
};

