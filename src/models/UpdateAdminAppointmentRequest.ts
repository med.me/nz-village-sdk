/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UpdateAdminAppointmentDto } from './UpdateAdminAppointmentDto';

export type UpdateAdminAppointmentRequest = {
  /**
   * Appointment data
   */
  appointment: UpdateAdminAppointmentDto;
};

