/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { VillageSettingsDto } from './VillageSettingsDto';

export type UpdateVillageSettingsRequest = {
  /**
   * Village settings
   */
  settings: VillageSettingsDto;
};

