/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type GroupTourScheduleTimeSlot = {
  /**
   * Timeslot start time in minutes from day start [0-1439]
   */
  start: number;
  /**
   * Timeslot end time in minutes from day start [1-1440]
   */
  end: number;
  /**
   * Timeslot availability status
   */
  active: boolean;
  /**
   * Timeslot lunch start time in minutes from day start [0-1439]
   */
  lunchStart: number;
  /**
   * Timeslot lunch end time in minutes from day start [1-1440]
   */
  lunchEnd: number;
};

