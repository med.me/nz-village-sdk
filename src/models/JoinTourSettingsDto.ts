/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JoinTourTimeRangeDto } from './JoinTourTimeRangeDto';
import type { TourPricesDto } from './TourPricesDto';

export type JoinTourSettingsDto = {
  /**
   * Hide Tour section in booking form
   */
  hideBookingForm: boolean;
  /**
   * Tour prices
   */
  tourPrices: TourPricesDto;
  /**
   * Private tour time slots
   */
  tourTimeRanges: Array<JoinTourTimeRangeDto>;
};

