/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type CreateClientDto = {
  /**
   * Client name, minimum 3 characters long
   */
  name: string;
  /**
   * Client phone number
   */
  phoneNumber: string;
  /**
   * Client email
   */
  email?: string;
};

