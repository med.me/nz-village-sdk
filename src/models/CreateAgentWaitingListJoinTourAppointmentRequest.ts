/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CreateAgentWaitingListJoinTourAppointmentDto } from './CreateAgentWaitingListJoinTourAppointmentDto';

export type CreateAgentWaitingListJoinTourAppointmentRequest = {
  /**
   * JOIN_A_TOUR appointment data
   */
  appointment: CreateAgentWaitingListJoinTourAppointmentDto;
};

