/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppointmentGuideDto = {
  /**
   * Contact person name
   */
  name: string;
  /**
   * Contact person phone number
   */
  phoneNumber: string;
  /**
   * Contact person email
   */
  email?: string;
};

