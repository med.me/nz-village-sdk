/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PelecardPaymentProviderSettingsDto } from './PelecardPaymentProviderSettingsDto';

export type PaymentProviderSettingsDto = {
  /**
   * Pelecard provider settings
   */
  pelecard: PelecardPaymentProviderSettingsDto;
  /**
   * Time allowed to make a payment, in minutes. After timeout tou will be automatically cancelled
   */
  paymentTimeout: any;
};

