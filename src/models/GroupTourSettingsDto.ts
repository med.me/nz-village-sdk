/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ActiveTimeRangeDto } from './ActiveTimeRangeDto';
import type { TimeRangeDto } from './TimeRangeDto';

export type GroupTourSettingsDto = {
  /**
   * Group tour total duration, including lunch
   */
  tourDuration: number;
  /**
   * Group tour guide rest time, for tours without lunch
   */
  guideRestDuration: number;
  /**
   * Group tour lunch time period
   */
  lunchPeriod: TimeRangeDto;
  /**
   * Group tour without lunch block period
   */
  noLunchToursBlockPeriod: ActiveTimeRangeDto;
  /**
   * Exclude JOIN_A_TOUR time period from group tour time range
   */
  excludeJoinTourPeriod: boolean;
  /**
   * Group tour lunch duration
   */
  lunchDuration: number;
  /**
   * Maximum restaurant capacity for serving guests
   */
  lunchCapacity: number;
  /**
   * Maximum allowed group tours with the same start time
   */
  maxSameStartTimeTourCount: number;
};

