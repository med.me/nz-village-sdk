/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { WeekScheduleDto } from './WeekScheduleDto';

export type ScheduleSettingsDto = {
  /**
   * Village schedule
   */
  weekSchedule: WeekScheduleDto;
};

