/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type UpdateResourceDto = {
  /**
   * Shows is entity active or not
   */
  active: boolean;
  /**
   * Resource name, minimum 3 characters long
   */
  name: string;
  /**
   * Resource type
   */
  resourceType: UpdateResourceDto.resourceType;
  /**
   * Resource phone number
   */
  phoneNumber?: string;
  /**
   * Resource email
   */
  email?: string;
  /**
   * Reserved for JOIN_A_TOUR
   */
  joinTourReserve?: boolean;
  /**
   * Resource user Id
   */
  user?: string;
};

export namespace UpdateResourceDto {

  /**
   * Resource type
   */
  export enum resourceType {
    INTERNAL_GUIDE = 'INTERNAL_GUIDE',
    EXTERNAL_GUIDE = 'EXTERNAL_GUIDE',
  }


}

