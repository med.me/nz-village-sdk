/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UserDto } from './UserDto';

export type ResourceDto = {
  /**
   * Creation time
   */
  readonly createdAt: string;
  /**
   * Time of last update
   */
  readonly updatedAt: string;
  /**
   * Identifier
   */
  id: string;
  /**
   * Shows is entity active or not
   */
  active: boolean;
  /**
   * Resource name, minimum 3 characters long
   */
  name: string;
  /**
   * Resource type
   */
  resourceType: ResourceDto.resourceType;
  /**
   * Resource phone number
   */
  phoneNumber?: string;
  /**
   * Resource email
   */
  email?: string;
  /**
   * Resource user
   */
  user?: UserDto;
  /**
   * Reserved for JOIN_A_TOUR
   */
  joinTourReserve?: boolean;
};

export namespace ResourceDto {

  /**
   * Resource type
   */
  export enum resourceType {
    INTERNAL_GUIDE = 'INTERNAL_GUIDE',
    EXTERNAL_GUIDE = 'EXTERNAL_GUIDE',
  }


}

