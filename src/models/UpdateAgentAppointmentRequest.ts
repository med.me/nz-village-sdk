/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UpdateAgentAppointmentDto } from './UpdateAgentAppointmentDto';

export type UpdateAgentAppointmentRequest = {
  /**
   * Appointment data
   */
  appointment: UpdateAgentAppointmentDto;
};

