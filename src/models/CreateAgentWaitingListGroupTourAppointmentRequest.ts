/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CreateAgentWaitingListGroupTourAppointmentDto } from './CreateAgentWaitingListGroupTourAppointmentDto';

export type CreateAgentWaitingListGroupTourAppointmentRequest = {
  /**
   * GROUP_TOUR appointment data
   */
  appointments: Array<CreateAgentWaitingListGroupTourAppointmentDto>;
};

