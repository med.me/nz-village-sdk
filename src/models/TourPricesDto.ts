/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PersonTourPriceDto } from './PersonTourPriceDto';

export type TourPricesDto = {
  /**
   * Tour prices for local tourists in cents
   */
  localPrices: Array<PersonTourPriceDto>;
  /**
   * Tour prices for foreign tourists in cents
   */
  foreignPrices: Array<PersonTourPriceDto>;
  /**
   * Tour prices with lunch in cents
   */
  filmingPrice: number;
};

