/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AuthLogin = {
  /**
   * User name
   */
  username: string;
  /**
   * User password
   */
  password: string;
};

