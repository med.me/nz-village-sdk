/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EntityScheduleDto } from './EntityScheduleDto';
import type { ResourceDto } from './ResourceDto';

export type ResourceScheduleDto = {
  /**
   * Resource data
   */
  resource: ResourceDto;
  /**
   * Resource data
   */
  schedule: Array<EntityScheduleDto>;
};

