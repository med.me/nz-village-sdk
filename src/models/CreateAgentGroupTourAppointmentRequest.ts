/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CreateAgentGroupTourAppointmentDto } from './CreateAgentGroupTourAppointmentDto';

export type CreateAgentGroupTourAppointmentRequest = {
  /**
   * GROUP_TOUR appointment data
   */
  appointments: Array<CreateAgentGroupTourAppointmentDto>;
};

