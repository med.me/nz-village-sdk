/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppointmentTouristPriceDto = {
  /**
   * Price for adult visitors
   */
  adultsPrice: number;
  /**
   * Price for seniors or students
   */
  seniorsPrice: number;
  /**
   * Price for children under 5 y.o.
   */
  childrenCat1Price: number;
  /**
   * Price for children 5-12 y.o.
   */
  childrenCat2Price: number;
};

