/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PageInfo } from './PageInfo';

export type PageResponse = {
  page: PageInfo;
  content: Array<string>;
};

