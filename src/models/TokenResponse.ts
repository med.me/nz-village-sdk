/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TokenResponse = {
  /**
   * Access token
   */
  readonly access_token: string;
  /**
   * Refresh token
   */
  readonly refresh_token: string;
  /**
   * Expiration time in minutes
   */
  readonly expires_in: number;
  /**
   * Token type
   */
  readonly token_type: string;
};

