/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { GroupTourScheduleTimeSlot } from './GroupTourScheduleTimeSlot';

export type GroupTourDateSlots = {
  /**
   * Time slots date in ISO8601 format. Example 2022-08-31
   */
  date: string;
  /**
   * Time slots for specific date
   */
  slots: Array<GroupTourScheduleTimeSlot>;
};

