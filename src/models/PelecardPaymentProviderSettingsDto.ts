/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PelecardPaymentProviderSettingsDto = {
  /**
   * Status of provider
   */
  active: boolean;
  /**
   * Terminal settings
   */
  terminal?: string;
  /**
   * User settings
   */
  user?: string;
  /**
   * Password settings
   */
  password?: string;
};

