/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EntityScheduleDto } from './EntityScheduleDto';

export type UpdateResourceScheduleRequest = {
  /**
   * Resource schedule
   */
  schedule: Array<EntityScheduleDto>;
};

