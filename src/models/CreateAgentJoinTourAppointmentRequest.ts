/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CreateAgentJoinTourAppointmentDto } from './CreateAgentJoinTourAppointmentDto';

export type CreateAgentJoinTourAppointmentRequest = {
  /**
   * JOIN_A_TOUR appointment data
   */
  appointment: CreateAgentJoinTourAppointmentDto;
};

