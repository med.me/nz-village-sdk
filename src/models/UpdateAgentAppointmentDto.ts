/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppointmentClientDto } from './AppointmentClientDto';
import type { AppointmentGuideDto } from './AppointmentGuideDto';
import type { AppointmentPriceDto } from './AppointmentPriceDto';
import type { AppointmentVisitorsDto } from './AppointmentVisitorsDto';

export type UpdateAgentAppointmentDto = {
  /**
   * Identifier
   */
  id: string;
  /**
   * Appointment start date and time in ISO8601 UTC format
   */
  start: string;
  /**
   * Appointment end date and time in ISO8601 UTC format
   */
  end: string;
  /**
   * Appointment client contact person
   */
  contactPerson?: AppointmentClientDto;
  /**
   * Appointment guide
   */
  guide?: AppointmentGuideDto;
  /**
   * Appointment status
   */
  status?: UpdateAgentAppointmentDto.status;
  /**
   * Number of visitors
   */
  visitors: AppointmentVisitorsDto;
  /**
   * Group name
   */
  groupName?: string;
  /**
   * Country of origin
   */
  originCountry: Array<any[]>;
  /**
   * Include lunch or not
   */
  withLunch: boolean;
  /**
   * Price
   */
  price?: AppointmentPriceDto;
  /**
   * Tour agent notes
   */
  agentNotes?: string;
};

export namespace UpdateAgentAppointmentDto {

  /**
   * Appointment status
   */
  export enum status {
    CONFIRMED = 'CONFIRMED',
    WAITING_LIST = 'WAITING_LIST',
    WAITING_FOR_PAYMENT = 'WAITING_FOR_PAYMENT',
    PAYMENT_FAILED = 'PAYMENT_FAILED',
    CANCELED = 'CANCELED',
    DELETED = 'DELETED',
  }


}

