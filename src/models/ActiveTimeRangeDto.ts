/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ActiveTimeRangeDto = {
  /**
   * Range start in minutes
   */
  start: number;
  /**
   * Range end in minutes
   */
  end: number;
  /**
   * Status of time range
   */
  active: boolean;
};

