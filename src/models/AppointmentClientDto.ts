/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AppointmentClientDto = {
  /**
   * Contact person name
   */
  name: string;
  /**
   * Contact person phone number
   */
  phoneNumber: string;
  /**
   * Contact person email
   */
  email?: string;
  /**
   * Identifier
   */
  id: string;
};

