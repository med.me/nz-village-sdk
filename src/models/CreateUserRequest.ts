/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CreateUserDto } from './CreateUserDto';

export type CreateUserRequest = {
  /**
   * User data
   */
  user: CreateUserDto;
};

