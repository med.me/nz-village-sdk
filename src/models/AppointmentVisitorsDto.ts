/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppointmentTouristVisitorsDto } from './AppointmentTouristVisitorsDto';

export type AppointmentVisitorsDto = {
  /**
   * Local visitors
   */
  localVisitors: AppointmentTouristVisitorsDto;
  /**
   * Foreign visitors
   */
  foreignVisitors: AppointmentTouristVisitorsDto;
};

