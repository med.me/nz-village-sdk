/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AppointmentTouristVisitorsDto } from './AppointmentTouristVisitorsDto';

export type AppointmentJoinTourVisitorsDto = {
  /**
   * Local visitors
   */
  localVisitors: AppointmentTouristVisitorsDto;
  /**
   * Foreign visitors
   */
  foreignVisitors: AppointmentTouristVisitorsDto;
};

