/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UserRole } from './UserRole';

export type UserDto = {
  /**
   * Creation time
   */
  readonly createdAt: string;
  /**
   * Time of last update
   */
  readonly updatedAt: string;
  /**
   * Identifier
   */
  id: string;
  /**
   * Shows is entity active or not
   */
  active: boolean;
  /**
   * User name, minimum 3 characters long, only letters, digits, "@" and "-" allowed
   */
  username: string;
  /**
   * User email
   */
  email: string;
  /**
   * User agency
   */
  agency?: string;
  /**
   * User full name
   */
  fullName?: string;
  /**
   * User permission roles
   */
  roles: Array<UserRole>;
};

