/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { TimeSlot } from './TimeSlot';

export type ResourceSlots = {
  /**
   * Resource ID
   */
  resource: string;
  /**
   * Time slots for resource
   */
  slots: Array<TimeSlot>;
};

