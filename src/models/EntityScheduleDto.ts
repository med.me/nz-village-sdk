/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type EntityScheduleDto = {
  /**
   * Schedule date in ISO8601 format. Example 2022-08-01
   */
  date: string;
  /**
   * Is day active or not
   */
  active: any;
};

