/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ActiveTimeRangeDto } from './ActiveTimeRangeDto';

export type WeekScheduleDto = {
  /**
   * Village monday schedule
   */
  mon: ActiveTimeRangeDto;
  /**
   * Village tuesday schedule
   */
  tue: ActiveTimeRangeDto;
  /**
   * Village wednesday schedule
   */
  wed: ActiveTimeRangeDto;
  /**
   * Village thursday schedule
   */
  thu: ActiveTimeRangeDto;
  /**
   * Village friday schedule
   */
  fri: ActiveTimeRangeDto;
  /**
   * Village saturday schedule
   */
  sat: ActiveTimeRangeDto;
  /**
   * Village sunday schedule
   */
  sun: ActiveTimeRangeDto;
};

