/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UpdateUserDto } from './UpdateUserDto';

export type UpdateUserRequest = {
  /**
   * User data
   */
  user: UpdateUserDto;
};

