/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PageInfo = {
  /**
   * Current page number
   */
  pageNumber: number;
  /**
   * Current page size
   */
  pageSize: number;
  /**
   * Total elements count for the current filter
   */
  totalElements: number;
  /**
   * Total page count for the current filter
   */
  totalPages: number;
};

