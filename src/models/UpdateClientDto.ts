/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type UpdateClientDto = {
  /**
   * Shows is entity active or not
   */
  active: boolean;
  /**
   * Client name, minimum 3 characters long
   */
  name: string;
  /**
   * Client phone number
   */
  phoneNumber: string;
  /**
   * Client email
   */
  email?: string;
};

