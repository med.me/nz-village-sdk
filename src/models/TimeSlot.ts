/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TimeSlot = {
  /**
   * Timeslot start time in minutes from day start [0-1439]
   */
  start: number;
  /**
   * Timeslot end time in minutes from day start [1-1440]
   */
  end: number;
};

