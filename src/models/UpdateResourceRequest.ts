/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UpdateResourceDto } from './UpdateResourceDto';

export type UpdateResourceRequest = {
  /**
   * Resource data
   */
  resource: UpdateResourceDto;
};

